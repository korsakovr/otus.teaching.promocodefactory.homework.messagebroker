﻿using Otus.Teaching.Pcf.Common;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Services.Interfaces
{
    public interface IPartnerPromoCodeService
    {
        public Task UpdateAppliedPromocodesAsync(Guid? id);
    }
}
